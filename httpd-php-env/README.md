# httpd24-php56-php72

A hard implementation for php56 && php72 with supervisord

## Getting Started

Please calm and breathe ... 

### Prerequisites

Install docker and kitematic for purposes to monitoring


### Installing

Build with docker 

```
docker build -t httpd-php-env .
```

(for me, hahaha)
```
docker tag httpd-php-env:latest arturoth/httpd-php-env:latest
docker push arturoth/httpd-php-env:latest
```

And for you, little leecher
```
docker run -t -i -p 80:80/tcp -p 9001:9001/tcp httpd-php-env /bin/bash
```

If you want to recover image
```
docker attach <id>
```

## Deployment

127.0.0.1:80, exposes your http sites-available

127.0.0.1:9001, exposes your supervisord 

##  Some stuff

To switching php56 <=> php72

Go to httpd-php-switch and lets start/restart. Show "Tail -F" and lets see this message:

```
HTTPD24_DEFAULT=PHP{VERSION}, restart httpd
```

In version must be 56 or 72 and after this, restart httpd.

## How to 

* [Docker-container-networking](https://docs.docker.com/config/containers/container-networking/) - Ports and more networking

## Authors

* **Arturo I** - *Initial work* - [arturoth](https://github.com/arturoth)

See also the list of [contributors](https://github.com/arturoth/cl/contributors) who participated in this project.

## License

This project is licensed under the GPL License - enjoy it! 

## Acknowledgments

* All your bases are belong to us.

* Cant defeat airman 

