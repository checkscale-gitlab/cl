#!/bin/bash
conf_php56=10-php56-php.conf
conf_php72=15-rh-php72-php.conf
libpath=/etc/httpd/conf.modules.d

function disablePhp(){
    conf_phpfile=$1
    disable=$2

    if [[ ${disable} == "true" ]];
    then
        cat $libpath/$conf_phpfile|sed -e "s/LoadModule/#LoadModule/g" >> $libpath/replace-$conf_phpfile
    else
        cat $libpath/$conf_phpfile|sed -e "s/#LoadModule/LoadModule/g" >> $libpath/replace-$conf_phpfile
    fi
    mv $libpath/replace-$conf_phpfile $libpath/$conf_phpfile
}

if [[ "${conf_phpfile}" == "15-rh-php72-php.conf" ]];
then
    libpath=/opt/rh/httpd24/root/etc/httpd/conf.modules.d
fi

if [[ $(grep "#LoadModule" $libpath/$conf_php72|wc -l) -eq 0 && $(grep "#LoadModule" $libpath/$conf_php56|wc -l) -eq 0 ]];
then
    disablePhp "$conf_php72" "true"
    echo "1.HTTPD24_DEFAULT=PHP56, restart httpd"
else
    if [[ $(grep "#LoadModule" $libpath/$conf_php72|wc -l) -ge 1 ]];
    then
        disablePhp "$conf_php72" "false"
        disablePhp "$conf_php56" "true"
        echo "2.HTTPD24_DEFAULT=PHP72, restart httpd"
    else
        disablePhp "$conf_php72" "true"
        disablePhp "$conf_php56" "false"
        echo "2.HTTPD24_DEFAULT=PHP56, restart httpd"
    fi
fi
