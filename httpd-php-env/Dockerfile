FROM centos:latest

MAINTAINER "arturoth" <arturoth@gmail.com>

ENV container docker
ENV path_data_php72 /var/lib/data/php-fpm72/
ENV path_data_php56 /var/lib/data/php-fpm56/
ENV path_conf_sa_httpd24 /opt/rh/httpd24/root/etc/httpd/sites-available/
ENV path_shared /var/apps/shared/

# install repos what i needed to build http+php env
RUN yum install -y centos-release-scl
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
RUN yum install -y https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm
RUN yum install -y http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm

# enable remi for php56
RUN yum-config-manager --enable remi-php56

# tools
RUN \
  rpm --rebuilddb && yum clean all && rm -rf /var/cache/yum && \
  yum update -y && \
  yum makecache fast && \
  yum -y install \
    epel-release \
    openssh-clients \
    iproute \
    at \
    curl \
    crontabs \
    cronolog \
    git \
    supervisor

# httpd24
RUN yum install -y httpd24u httpd24u-devel httpd24u-filesystem httpd24u-mod_ldap httpd24u-mod_proxy_html httpd24u-mod_security2 httpd24u-mod_security2-mlogc httpd24u-mod_session httpd24u-mod_ss httpd24u-mod_xsendfile httpd24u-tools

RUN mkdir -p $path_data_php72 \
    && mkdir -p $path_data_php56 \
    && mkdir -p $path_shared \
    && mkdir -p $path_conf_sa_httpd24 \
    && mkdir -p /var/log/php-fpm \
    && chown -R apache:apache $path_data_php72 \
    && chown -R apache:apache $path_data_php56

# install php56
RUN yum install -y php56-php-fpm.x86_64 php56.x86_64 php56-build.x86_64 php56-mysqlnd-qc-panel.noarch php56-php-cli.x86_64 php56-php-common.x86_64 php56-php-dba.x86_64 php56-php-dbg.x86_64 php56-php-devel.x86_64 php56-php-embedded.x86_64 php56-php-enchant.x86_64 php56-php-gd.x86_64 php56-php-geos.x86_64 php56-php-gmp.x86_64 php56-php-imap.x86_64 php56-php-interbase.x86_64 php56-php-intl.x86_64 php56-php-ldap.x86_64 php56-php-libvirt.x86_64 php56-php-litespeed.x86_64 php56-php-lz4.x86_64 php56-php-magickwand.x86_64 php56-php-maxminddb.x86_64 php56-php-mbstring.x86_64 php56-php-mcrypt.x86_64 php56-php-mssql.x86_64 php56-php-mysqlnd.x86_64 php56-php-opcache.x86_64 php56-php-pdo.x86_64 php56-php-pdo.x86_64 php56-php-pgsql.x86_64 php56-php-process.x86_64 php56-php-pspell.x86_64 php56-php-snmp.x86_64 php56-php-soap.x86_64 php56-php-tidy.x86_64 php56-php-twig.x86_64 php56-php-xcache.x86_64 php56-php-xml.x86_64 php56-php-xmlrpc.x86_64

# install php72
RUN yum install -y rh-php72-php-fpm.x86_64 rh-php72.x86_64 rh-php72-build.x86_64 rh-php72-php.x86_64 rh-php72-php-bcmath.x86_64 rh-php72-php-cli.x86_64 rh-php72-php-common.x86_64 rh-php72-php-dba.x86_64 rh-php72-php-dbg.x86_64 rh-php72-php-devel.x86_64 rh-php72-php-embedded.x86_64 rh-php72-php-enchant.x86_64 rh-php72-php-gd.x86_64 rh-php72-php-gmp.x86_64 rh-php72-php-intl.x86_64 rh-php72-php-json.x86_64 rh-php72-php-ldap.x86_64 rh-php72-php-mbstring.x86_64 rh-php72-php-mysqlnd.x86_64 rh-php72-php-mysqlnd.x86_64 rh-php72-php-pdo.x86_64 rh-php72-php-pear.noarch rh-php72-php-pecl-apcu.x86_64 rh-php72-php-pecl-apcu-devel.x86_64 rh-php72-php-pgsql.x86_64 rh-php72-php-process.x86_64 rh-php72-php-pspell.x86_64 rh-php72-php-snmp.x86_64 rh-php72-php-recode.x86_64 rh-php72-php-soap.x86_64 rh-php72-php-xml.x86_64 rh-php72-php-xmlrpc.x86_64 rh-php72-php-zip.x86_64 rh-php72-runtime.x86_64 rh-php72-scldevel.x86_64  

# setting php72
RUN alternatives --install /usr/bin/php72 php72 /opt/rh/rh-php72/root/bin/php 4 \
    && alternatives --install /usr/bin/phpize72 phpize72 /opt/rh/rh-php72/root/bin/phpize 4 \
    && alternatives --install /usr/bin/php-cgi72 php-cgi72 /opt/rh/rh-php72/root/bin/php-cgi 4 \
    && alternatives --install /usr/bin/php-config72 php-config72 /opt/rh/rh-php72/root/bin/php-config 4 \
    && alternatives --install /usr/bin/pear72 pear72 /opt/rh/rh-php72/root/bin/pear 4 \
    && alternatives --install /usr/bin/phar72 phar72 /opt/rh/rh-php72/root/bin/phar 4 \
    && alternatives --install /usr/bin/pecl72 pecl72 /opt/rh/rh-php72/root/bin/pecl 4

RUN alternatives --set php72 /opt/rh/rh-php72/root/bin/php \
    && alternatives --set phpize72 /opt/rh/rh-php72/root/bin/phpize \
    && alternatives --set php-cgi72 /opt/rh/rh-php72/root/bin/php-cgi \
    && alternatives --set pear72 /opt/rh/rh-php72/root/bin/pear \
    && alternatives --set pecl72 /opt/rh/rh-php72/root/bin/pecl \
    && alternatives --set phar72 /opt/rh/rh-php72/root/bin/phar

# setting php56
RUN alternatives --install /usr/bin/php56 php56 /opt/remi/php56/root/bin/php 4 \
    && alternatives --install /usr/bin/phpize56 phpize56 /opt/remi/php56/root/bin/phpize 4 \
    && alternatives --install /usr/bin/php-cgi56 php-cgi56 /opt/remi/php56/root/bin/php-cgi 4 \
    && alternatives --install /usr/bin/phar56 phar56 /opt/remi/php56/root/bin/phar 4 \
    && alternatives --install /usr/bin/pecl56 pecl56 /opt/remi/php56/root/bin/pecl 4 \
    && alternatives --install /usr/bin/pear56 pear56 /opt/remi/php56/root/bin/pear 4 \
    && alternatives --install /usr/bin/php-config56 php-config56 /opt/remi/php56/root/usr/php-config 4 \
    && alternatives --install /usr/bin/peardev56 peardev56 /opt/remi/php56/root/bin/peardev 4

RUN alternatives --set php56 /opt/remi/php56/root/bin/php \
    && alternatives --set phpize56 /opt/remi/php56/root/bin/phpize \
    && alternatives --set php-cgi56 /opt/remi/php56/root/bin/php-cgi \
    && alternatives --set pear56 /opt/remi/php56/root/bin/pear \
    && alternatives --set pecl56 /opt/remi/php56/root/bin/pecl \
    && alternatives --set phar56 /opt/remi/php56/root/bin/phar

# setting php default
RUN alternatives --install /usr/bin/php php /opt/rh/rh-php72/root/bin/php 1 \
    && alternatives --install /usr/bin/phpize phpize /opt/rh/rh-php72/root/bin/phpize 1 \
    && alternatives --install /usr/bin/php-cgi php-cgi /opt/rh/rh-php72/root/bin/php-cgi 1 \
    && alternatives --install /usr/bin/php-config php-config /opt/rh/rh-php72/root/bin/php-config 1 \
    && alternatives --install /usr/bin/pear pear /opt/rh/rh-php72/root/bin/pear 1 \
    && alternatives --install /usr/bin/phar phar /opt/rh/rh-php72/root/bin/phar 1 \
    && alternatives --install /usr/bin/pecl pecl /opt/rh/rh-php72/root/bin/pecl 1

RUN alternatives --install /usr/bin/php php /opt/remi/php56/root/bin/php 2 \
    && alternatives --install /usr/bin/phpize phpize /opt/remi/php56/root/bin/phpize 2 \
    && alternatives --install /usr/bin/php-cgi php-cgi /opt/remi/php56/root/bin/php-cgi 2 \
    && alternatives --install /usr/bin/phar phar /opt/remi/php56/root/bin/phar 2 \
    && alternatives --install /usr/bin/pecl pecl /opt/remi/php56/root/bin/pecl 2 \
    && alternatives --install /usr/bin/pear pear /opt/remi/php56/root/bin/pear 2 \
    && alternatives --install /usr/bin/php-config php-config /opt/remi/php/root/usr/php-config 2 \
    && alternatives --install /usr/bin/peardev peardev /opt/remi/php56/root/bin/peardev 2

RUN alternatives --set php /opt/remi/php56/root/bin/php \
    && alternatives --set phpize /opt/remi/php56/root/bin/phpize \
    && alternatives --set php-cgi /opt/remi/php56/root/bin/php-cgi \
    && alternatives --set pear /opt/remi/php56/root/bin/pear \
    && alternatives --set pecl /opt/remi/php56/root/bin/pecl \
    && alternatives --set phar /opt/remi/php56/root/bin/phar

# config
RUN \
  echo -e "StrictHostKeyChecking no" >> /etc/ssh/ssh_config && \
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
  chown apache /usr/local/bin/composer && composer --version && \
  yum clean all && rm -rf /tmp/yum* 

# we want some config changes
ADD config/supervisord/supervisord.conf /etc/supervisord.d/
ADD config/php56/php.ini /opt/remi/php56/root/etc
ADD config/php56/www.conf /opt/remi/php56/root/etc/php-fpm.d
ADD config/php56/php-fpm.conf /opt/remi/php56/root/etc
ADD config/php72/php.ini /etc/opt/rh/rh-php72
ADD config/php72/www.conf /etc/opt/rh/rh-php72/php-fpm.d
ADD config/php72/php-fpm.conf /etc/opt/rh/rh-php72
#ADD config/httpd24/httpd.conf /opt/rh/httpd24/root/etc/httpd/conf
ADD config/httpd24/httpd.conf /etc/httpd/conf

# add files to review
ADD config/httpd24/sites-available/content.online.conf /opt/rh/httpd24/root/etc/httpd/sites-available
ADD files /

RUN ln -s /opt/rh/httpd24/root/etc/httpd/conf.d/rh-php72-php.conf /etc/httpd/conf.d/ \
    && ln -s /opt/rh/httpd24/root/etc/httpd/conf.modules.d/15-rh-php72-php.conf /etc/httpd/conf.modules.d/ \
    && ln -s /opt/rh/httpd24/root/etc/httpd/modules/librh-php72-php7.so /etc/httpd/modules/ 

# next steps to review 
RUN cd /var/www/html/reportautomator/adhp \
    && ln -s /var/apps/shared/report_automator_core_v2/smarty/ smarty \
    && chgrp apache /var/www/html/reportautomator/adhp/log \
    && chgrp apache /var/www/html/reportautomator/adhp/template_c \
    && chmod 0775 /var/www/html/reportautomator/adhp/log \
    && chmod 0775 /var/www/html/reportautomator/adhp/template_c

# Configure Services and Port
ADD scripts/start.sh /root/
ADD scripts/do_php_switch.sh /root/

#CMD ["/bin/sh /root/start.sh"]

EXPOSE 80 22 9001
