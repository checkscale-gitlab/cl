# docker-java-env

A very greatful implementation from java6 to java12 with gradle, maven and ant (Remember, if u want to use you neeed to switch to java8 or superior).

## Getting Started

Versions of java in next table.

### Java

| Version               | Description                           | Label                 |
| --------------------- | ------------------------------------- | --------------------- |
| JAVA 6                | jdk-6u45-linux-x64-rpm.bin            | required              |
| JAVA 7                | jdk-7u80-linux-x64.rpm  				| required              |
| JAVA 8                | jdk-8u211-linux-x64.rpm 				| required              |
| JAVA 11               | jdk-11.0.3_linux-x64_bin.rpm 			| required              |
| JAVA 12			    | jdk-12.0.1_linux-x64_bin.rpm 			| required              |

### Prerequisites

Install docker and kitematic for purposes to monitoring


### Installing

Build with docker 

```
docker build -t docker-java-env .
```

(for me, hahaha)
```
docker tag docker-java-env:latest arturoth/docker-java-env:latest
docker push arturoth/docker-java-env:latest
```

And for you, little leecher
```
docker run -t -i -p 9001:9001/tcp docker-java-env /bin/bash
```

If you want to recover image
```
docker attach <id>
```

## Deployment

127.0.0.1:9001, exposes your supervisord 

##  Some stuff

Coming soon.

## How to 

* [Docker-container-networking](https://docs.docker.com/config/containers/container-networking/) - Ports and more networking

## Authors

* **Arturo I** - *Initial work* - [arturoth](https://github.com/arturoth)

See also the list of [contributors](https://github.com/arturoth/cl/contributors) who participated in this project.

## License

[GPL](//www.gnu.org/licenses/quick-guide-gplv3.html)

## Acknowledgments

* All your bases are belong to us.

* Cant defeat airman 

